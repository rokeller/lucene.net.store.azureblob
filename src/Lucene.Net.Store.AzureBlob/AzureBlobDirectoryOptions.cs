namespace Lucene.Net.Store
{
    public class AzureBlobDirectoryOptions
    {
        public bool CacheSegmentsGen { get; set; }
    }
}